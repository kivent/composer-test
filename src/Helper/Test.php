<?php
declare (strict_types=1);
namespace Transn\Helper;

class Test
{
    /**
     * 获取uuid
     * @return string
     * Create BY kivent.zou <kivent.zou@transn.com>
     * DateTime 2023/12/21 20:04
     */
    public static function uuid(): string
    {
        return md5(uuid_create(1));
    }
}